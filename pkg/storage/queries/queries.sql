-- name: WOZObjectListAsc :many
SELECT objs.* FROM (
    SELECT woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, internal_id,
        owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
        occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
        ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
    FROM digilab_demo_fwoz.woz_objects
    LEFT JOIN digilab_demo_fwoz.stakeholders as owners
        ON owners.id=woz_objects.stakeholder_owner
    LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
        ON occupants.id=woz_objects.stakeholder_occupant
    LEFT JOIN digilab_demo_fwoz.object_value as ov
        ON ov.woz_object_id=woz_objects.id
    WHERE woz_objects.internal_id > (
        SELECT sub_qry_woz_objects.internal_id
        FROM digilab_demo_fwoz.woz_objects as sub_qry_woz_objects
        WHERE sub_qry_woz_objects.id=$1
    )
    ORDER BY woz_objects.internal_id ASC
    LIMIT $2
) objs ORDER BY objs.internal_id DESC;

-- name: WOZObjectListAscBsn :many
SELECT objs.* FROM (
    SELECT woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, internal_id,
        owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
        occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
        ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
    FROM digilab_demo_fwoz.woz_objects
    LEFT JOIN digilab_demo_fwoz.stakeholders as owners
        ON owners.id=woz_objects.stakeholder_owner
    LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
        ON occupants.id=woz_objects.stakeholder_occupant
    LEFT JOIN digilab_demo_fwoz.object_value as ov
        ON ov.woz_object_id=woz_objects.id
    WHERE woz_objects.internal_id > (
        SELECT sub_qry_woz_objects.internal_id
        FROM digilab_demo_fwoz.woz_objects as sub_qry_woz_objects
        WHERE sub_qry_woz_objects.id=$1
    )
AND owners.bsn = $3::text OR occupants.bsn = $3::text
    ORDER BY woz_objects.internal_id ASC
    LIMIT $2
) objs ORDER BY objs.internal_id DESC;

-- name: WOZObjectListDesc :many
SELECT woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, woz_objects.internal_id,
    owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
    occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
    ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
FROM digilab_demo_fwoz.woz_objects
LEFT JOIN digilab_demo_fwoz.stakeholders as owners
    ON owners.id=woz_objects.stakeholder_owner
LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
    ON occupants.id=woz_objects.stakeholder_occupant
LEFT JOIN digilab_demo_fwoz.object_value as ov
    ON ov.woz_object_id=woz_objects.id
WHERE woz_objects.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fwoz.woz_objects as sub_qry_woz_objects
    WHERE sub_qry_woz_objects.id=$1
), $2)
ORDER BY woz_objects.internal_id DESC
LIMIT $3;

-- name: WOZObjectListDescBsn :many
SELECT woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, woz_objects.internal_id,
    owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
    occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
    ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
FROM digilab_demo_fwoz.woz_objects
LEFT JOIN digilab_demo_fwoz.stakeholders as owners
    ON owners.id=woz_objects.stakeholder_owner
LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
    ON occupants.id=woz_objects.stakeholder_occupant
LEFT JOIN digilab_demo_fwoz.object_value as ov
    ON ov.woz_object_id=woz_objects.id
WHERE woz_objects.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fwoz.woz_objects as sub_qry_woz_objects
    WHERE sub_qry_woz_objects.id=$1
), $2)
AND owners.bsn = $4::text OR occupants.bsn = $4::text
ORDER BY woz_objects.internal_id DESC
LIMIT $3;

-- name: WOZObjectGet :one
SELECT woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, woz_objects.internal_id,
    owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
    occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
    ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
FROM digilab_demo_fwoz.woz_objects
LEFT JOIN digilab_demo_fwoz.stakeholders as owners
    ON owners.id=woz_objects.stakeholder_owner
LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
    ON occupants.id=woz_objects.stakeholder_occupant
LEFT JOIN digilab_demo_fwoz.object_value as ov
    ON ov.woz_object_id=woz_objects.id
WHERE woz_objects.id = $1;

-- name: WOZObjectCreate :exec
INSERT INTO digilab_demo_fwoz.woz_objects
(id, stakeholder_owner, stakeholder_occupant, addressable_object_id, registered_people, type)
VALUES (
    $1, $2, $3, $4, $5, $6
);

-- name: StakeholderCreate :exec
INSERT INTO digilab_demo_fwoz.stakeholders
(id, bsn, rsin, type)
VALUES (
    $1, $2, $3, $4
);

-- name: WOZObjectValueList :many
SELECT id, woz_object_id, value, valuation_at, effective_at
FROM digilab_demo_fwoz.object_value
WHERE woz_object_id = $1;

-- name: WOZObjectValueCreate :exec
INSERT INTO digilab_demo_fwoz.object_value
(id, woz_object_id, value, valuation_at, effective_at)
VALUES (
    $1, $2, $3, $4, $5
);
