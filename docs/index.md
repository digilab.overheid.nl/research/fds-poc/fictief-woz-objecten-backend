---
title : "FDS - Fictieve WOZ Objecten backend"
description: "Fictional API to manage WOZ Objects"
date: 2023-09-28T09:14:40+02:00
draft: false
toc: true
---

## Running locally

# Running locally in docker
To run the web server in docker on port 9100

Run:
```shell
make build

make up
```

To stop the developement server
```shell
make down
```

To view the logs of the server
```shell
make logs
```
