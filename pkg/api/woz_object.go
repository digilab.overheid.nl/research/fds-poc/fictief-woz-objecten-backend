//nolint:nlreturn // makes code less readable
package api

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/meta"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/pagination"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/storage/queries/generated"
)

func (a *API) WOZObjectList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	bsn := r.URL.Query().Get("bsn")

	var records []*queries.WOZObjectGetRow

	if bsn == "" {
		switch md.IsASC() {
		case true:
			records, err = a.wozObjectListAsc(r.Context(), md)
		case false:
			records, err = a.wozObjectListDesc(r.Context(), md)
		}
	} else {
		switch md.IsASC() {
		case true:
			records, err = a.wozObjectListAscBsn(r.Context(), md, bsn)
		case false:
			records, err = a.wozObjectListDescBsn(r.Context(), md, bsn)
		}
	}

	if err != nil {
		writeError(w, err)
		return
	}

	objects := adapter.ToWOZObjects(records)

	response, err := pagination.Build(objects, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) wozObjectListAsc(ctx context.Context, md *meta.Data) ([]*queries.WOZObjectGetRow, error) {
	params := &queries.WOZObjectListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.WOZObjectListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	people := make([]*queries.WOZObjectGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.WOZObjectGetRow)(records[idx]))
	}

	return people, nil
}

func (a *API) wozObjectListDesc(ctx context.Context, md *meta.Data) ([]*queries.WOZObjectGetRow, error) {
	params := &queries.WOZObjectListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.WOZObjectListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	people := make([]*queries.WOZObjectGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.WOZObjectGetRow)(records[idx]))
	}

	return people, nil
}
func (a *API) wozObjectListAscBsn(ctx context.Context, md *meta.Data, bsn string) ([]*queries.WOZObjectGetRow, error) {
	params := &queries.WOZObjectListAscBsnParams{
		ID:      md.FirstID,
		Limit:   int32(md.PerPage + 1),
		Column3: bsn,
	}

	records, err := a.db.Queries.WOZObjectListAscBsn(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	people := make([]*queries.WOZObjectGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.WOZObjectGetRow)(records[idx]))
	}

	return people, nil
}

func (a *API) wozObjectListDescBsn(ctx context.Context, md *meta.Data, bsn string) ([]*queries.WOZObjectGetRow, error) {
	params := &queries.WOZObjectListDescBsnParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
		Column4:    bsn,
	}

	records, err := a.db.Queries.WOZObjectListDescBsn(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	people := make([]*queries.WOZObjectGetRow, 0, len(records))
	for idx := range records {
		people = append(people, (*queries.WOZObjectGetRow)(records[idx]))
	}

	return people, nil
}

func (a *API) WOZObjectGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, fmt.Errorf("uuid parse: %w", err))
		return
	}

	record, err := a.db.Queries.WOZObjectGet(r.Context(), id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		writeError(w, fmt.Errorf("woz object get: %w", err))
		return
	}

	recordValues, err := a.db.Queries.WOZObjectValueList(r.Context(), id)
	if err != nil {
		writeError(w, fmt.Errorf("woz object values list: %w", err))
		return
	}

	object := adapter.ToWOZObject(record)
	object.Values = adapter.ToWOZObjectValues(recordValues)

	if err := json.NewEncoder(w).Encode(object); err != nil {
		writeError(w, fmt.Errorf("encoding failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) WOZObjectGetMany(w http.ResponseWriter, r *http.Request) {
	var data struct {
		IDs []uuid.UUID `json:"ids"`
	}
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		writeError(w, err)
		return
	}

	if len(data.IDs) > 1000 {
		writeError(w, errors.New("too many IDs requested, should be at most 1000"))
		return
	}

	var objects []*model.WOZObject

	if len(data.IDs) != 0 {
		records, err := a.WOZObjectGetManyDB(r.Context(), data.IDs)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			writeError(w, fmt.Errorf("woz object get many: %w", err))
			return
		}

		objects = adapter.ToWOZObjects(records)
	}

	if err := json.NewEncoder(w).Encode(objects); err != nil {
		writeError(w, fmt.Errorf("encoding failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

const query = `
	SELECT DISTINCT ON (woz_object_id) woz_objects.id, stakeholder_owner, stakeholder_occupant, addressable_object_id, created_at, registered_people, woz_objects.type, woz_objects.internal_id,
		owners.bsn as owner_bsn, owners.rsin as owner_rsin, owners.type as owner_type,
		occupants.bsn as occupant_bsn, occupants.rsin as occupant_rsin, occupants.type as occupant_type,
		ov.id as ov_id, ov.woz_object_id, ov.value, ov.valuation_at, ov.effective_at
	FROM digilab_demo_fwoz.woz_objects
	LEFT JOIN digilab_demo_fwoz.stakeholders as owners
		ON owners.id=woz_objects.stakeholder_owner
	LEFT JOIN digilab_demo_fwoz.stakeholders as occupants
		ON occupants.id=woz_objects.stakeholder_occupant
	LEFT JOIN digilab_demo_fwoz.object_value as ov
		ON ov.woz_object_id=woz_objects.id
	WHERE woz_objects.id = ANY($1::uuid[])
	ORDER BY woz_object_id, ov.valuation_at DESC;
`

func (a *API) WOZObjectGetManyDB(ctx context.Context, dollar_1 []uuid.UUID) ([]*queries.WOZObjectGetRow, error) {

	rows, err := a.db.DB.QueryContext(ctx, query, pq.Array(dollar_1))
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	items := []*queries.WOZObjectGetRow{}
	for rows.Next() {
		var i queries.WOZObjectGetRow
		if err := rows.Scan(
			&i.ID,
			&i.StakeholderOwner,
			&i.StakeholderOccupant,
			&i.AddressableObjectID,
			&i.CreatedAt,
			&i.RegisteredPeople,
			&i.Type,
			&i.InternalID,
			&i.OwnerBsn,
			&i.OwnerRsin,
			&i.OwnerType,
			&i.OccupantBsn,
			&i.OccupantRsin,
			&i.OccupantType,
			&i.OvID,
			&i.WozObjectID,
			&i.Value,
			&i.ValuationAt,
			&i.EffectiveAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

func (a *API) WOZObjectCreate(w http.ResponseWriter, r *http.Request) {
	object := &model.WOZObject{
		ID: uuid.New(),
	}
	if err := json.NewDecoder(r.Body).Decode(object); err != nil {
		writeError(w, err)
		return
	}

	if object.StakeholderOwner != nil {
		object.StakeholderOwner.ID = uuid.New()
		if err := a.db.Queries.StakeholderCreate(r.Context(), adapter.FromStakeholderCreate(object.StakeholderOwner)); err != nil {
			writeError(w, err)
			return
		}
	}

	if object.StakeholderOccupant != nil {
		object.StakeholderOccupant.ID = uuid.New()
		if err := a.db.Queries.StakeholderCreate(r.Context(), adapter.FromStakeholderCreate(object.StakeholderOccupant)); err != nil {
			writeError(w, err)
			return
		}
	}

	record, err := adapter.FromWOZObjectCreate(object)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.WOZObjectCreate(r.Context(), record); err != nil {
		writeError(w, err)
		return
	}

	for idx := range object.Values {
		id := object.Values[idx].ID

		if id == uuid.Nil { // ID is empty, lets generate one
			id = uuid.New()
		}

		params := adapter.FromWOZObjectValueCreate(&model.WOZObjectValue{
			ID:          id,
			WOZObjectID: object.ID,
			Value:       object.Values[idx].Value,
			ValuationAt: object.Values[idx].ValuationAt,
			EffectiveAt: object.Values[idx].EffectiveAt,
		})

		if err := a.db.Queries.WOZObjectValueCreate(r.Context(), params); err != nil {
			writeError(w, err)
			return
		}
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) WOZObjectValueCreate(w http.ResponseWriter, r *http.Request) {
	value := &model.WOZObjectValue{ID: uuid.New()}
	if err := json.NewDecoder(r.Body).Decode(value); err != nil {
		writeError(w, err)
		return
	}

	params := adapter.FromWOZObjectValueCreate(value)

	if err := a.db.Queries.WOZObjectValueCreate(r.Context(), params); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
