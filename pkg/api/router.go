package api

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func Router(api *API) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(Logger)
	r.Route("/woz-objects", func(r chi.Router) {
		r.Get("/", api.WOZObjectList)
		r.Get("/{id}", api.WOZObjectGet)
		r.Post("/", api.WOZObjectCreate)
	})

	r.Get("/woz-objects-many", api.WOZObjectGetMany)
	r.Post("/woz-objects-many", api.WOZObjectGetMany)

	r.Route("/woz-object-values", func(r chi.Router) {
		r.Post("/", api.WOZObjectValueCreate)
	})

	return r
}

func writeError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func Logger(next http.Handler) http.Handler {
	return middleware.RequestLogger(&logFormatter{Logger: log.New(os.Stdout, "", log.LstdFlags)})(next)
}

type logFormatter struct {
	Logger middleware.LoggerInterface
}

type logEntry struct {
	Logger  middleware.LoggerInterface
	request *http.Request
	buf     *bytes.Buffer
}

func (l *logEntry) Write(status, amount int, _ http.Header, elapsed time.Duration, _ interface{}) {
	fmt.Fprintf(l.buf, "%03d %dB in %s", status, amount, elapsed)
	l.Logger.Print(l.buf.String())
}

func (l *logEntry) Panic(v interface{}, _ []byte) {
	middleware.PrintPrettyStack(v)
}

func (l *logFormatter) NewLogEntry(r *http.Request) middleware.LogEntry { //nolint:ireturn // necessary to satisfy interface
	entry := &logEntry{
		Logger:  l.Logger,
		request: r,
		buf:     &bytes.Buffer{},
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}

	fmt.Fprintf(entry.buf, "%s://%s%s %s\" ", scheme, r.Host, r.RequestURI, r.Proto)

	return entry
}
