BEGIN transaction;

ALTER TABLE digilab_demo_fwoz.interested_parties
RENAME TO stakeholders;

ALTER TABLE digilab_demo_fwoz.woz_objects
DROP CONSTRAINT fk_interested_party;

ALTER TABLE digilab_demo_fwoz.woz_objects
DROP COLUMN interested_party;

ALTER TABLE digilab_demo_fwoz.woz_objects
RENAME COLUMN interested_owner TO stakeholder_owner;

ALTER TABLE digilab_demo_fwoz.woz_objects
ADD COLUMN registered_people int default 0 NOT NULL;

ALTER TABLE digilab_demo_fwoz.stakeholders
ALTER COLUMN type SET NOT NULL;

COMMIT;
