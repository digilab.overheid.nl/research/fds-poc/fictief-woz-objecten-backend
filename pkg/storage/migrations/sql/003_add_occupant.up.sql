BEGIN TRANSACTION;

ALTER TABLE digilab_demo_fwoz.woz_objects
ADD COLUMN stakeholder_occupant UUID;

ALTER TABLE digilab_demo_fwoz.woz_objects
ADD CONSTRAINT fk_stakeholder_occupant FOREIGN KEY(stakeholder_occupant) REFERENCES digilab_demo_fwoz.stakeholders(id);

CREATE TYPE woz_object_type AS ENUM ('residential', 'non_residential');

ALTER TABLE digilab_demo_fwoz.woz_objects
ADD COLUMN type woz_object_type NOT NULL;

COMMIT
