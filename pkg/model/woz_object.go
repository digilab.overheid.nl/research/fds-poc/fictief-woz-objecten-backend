package model

import (
	"errors"
	"time"

	"github.com/google/uuid"
)

type StakeholderType string

type WOZObjectType string

const (
	NaturalPerson    StakeholderType = "naturalPerson"
	NonNaturalPerson StakeholderType = "nonNaturalPerson"

	Residential    WOZObjectType = "residential"
	NonResidential WOZObjectType = "non-residential"
)

var ErrUnknownObjectType = errors.New("unknown object type")

type WOZObject struct {
	ID                  uuid.UUID        `json:"id"`
	AddressableObjectID uuid.UUID        `json:"addressableObjectId"`
	StakeholderOwner    *Stakeholder     `json:"stakeholderOwner,omitempty"`
	StakeholderOccupant *Stakeholder     `json:"stakeholderOccupant,omitempty"`
	Values              []WOZObjectValue `json:"values,omitempty"`
	RegisteredPeople    int              `json:"registeredPeople"`
	Type                WOZObjectType    `json:"type"`
	CreatedAt           time.Time        `json:"createdAt"`
}

func (object WOZObject) GetID() uuid.UUID { //nolint: gocritic
	return object.ID
}

type Stakeholder struct {
	ID   uuid.UUID       `json:"id"`
	BSN  string          `json:"burgerServiceNummer,omitempty"`
	RSIN string          `json:"rsin,omitempty"`
	Type StakeholderType `json:"type"`
}

type WOZObjectValue struct {
	ID          uuid.UUID  `json:"id"`
	WOZObjectID uuid.UUID  `json:"wozObjectId"`
	Value       int        `json:"value"`
	ValuationAt time.Time  `json:"valuationAt"`
	EffectiveAt *time.Time `json:"effectiveAt,omitempty"`
}
