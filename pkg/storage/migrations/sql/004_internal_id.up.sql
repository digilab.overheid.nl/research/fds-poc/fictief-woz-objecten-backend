BEGIN TRANSACTION;

ALTER TABLE digilab_demo_fwoz.woz_objects
ADD COLUMN internal_id SERIAL NOT NULL;

COMMIT;
