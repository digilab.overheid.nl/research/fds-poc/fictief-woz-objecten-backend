package adapter

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-backend/pkg/storage/queries/generated"
)

func ToWOZObject(record *queries.WOZObjectGetRow) *model.WOZObject {
	var owner *model.Stakeholder
	if record.StakeholderOwner.Valid {
		owner = &model.Stakeholder{
			ID:   record.StakeholderOwner.UUID,
			BSN:  record.OwnerBsn.String,
			RSIN: record.OwnerRsin.String,
			Type: model.StakeholderType(record.OwnerType.String),
		}
	}

	var occupant *model.Stakeholder
	if record.StakeholderOccupant.Valid {
		occupant = &model.Stakeholder{
			ID:   record.StakeholderOccupant.UUID,
			BSN:  record.OccupantBsn.String,
			RSIN: record.OccupantRsin.String,
			Type: model.StakeholderType(record.OccupantType.String),
		}
	}

	var values []model.WOZObjectValue

	if record.OvID.Valid {
		var effectiveAt *time.Time
		if record.EffectiveAt.Valid {
			effectiveAt = &record.EffectiveAt.Time
		}

		values = append(values, model.WOZObjectValue{
			ID:          record.OvID.UUID,
			WOZObjectID: record.ID,
			Value:       int(record.Value.Int32),
			ValuationAt: record.ValuationAt.Time,
			EffectiveAt: effectiveAt,
		})
	}

	var objectType model.WOZObjectType

	switch record.Type {
	case queries.WozObjectTypeNonResidential:
		objectType = model.NonResidential
	case queries.WozObjectTypeResidential:
		objectType = model.Residential
	}

	return &model.WOZObject{
		ID:                  record.ID,
		StakeholderOwner:    owner,
		StakeholderOccupant: occupant,
		AddressableObjectID: record.AddressableObjectID,
		RegisteredPeople:    int(record.RegisteredPeople),
		Type:                objectType,
		Values:              values,
		CreatedAt:           record.CreatedAt.Time,
	}
}

func ToWOZObjects(records []*queries.WOZObjectGetRow) []*model.WOZObject {
	exists := make(map[uuid.UUID]*model.WOZObject, len(records))
	objects := make([]*model.WOZObject, 0, len(records))

	for _, record := range records {
		woz := ToWOZObject(record)

		if m, ok := exists[woz.ID]; !ok {
			exists[woz.ID] = woz
			objects = append(objects, woz)
		} else {
			m.Values = append(m.Values, woz.Values...)
		}
	}

	return objects
}

func FromWOZObjectCreate(object *model.WOZObject) (*queries.WOZObjectCreateParams, error) {
	var ownerID uuid.NullUUID
	if object.StakeholderOwner != nil {
		ownerID = uuid.NullUUID{UUID: object.StakeholderOwner.ID, Valid: true}
	}

	var occupantID uuid.NullUUID
	if object.StakeholderOccupant != nil {
		occupantID = uuid.NullUUID{UUID: object.StakeholderOccupant.ID, Valid: true}
	}

	var objectType queries.WozObjectType

	switch object.Type {
	case model.NonResidential:
		objectType = queries.WozObjectTypeNonResidential
	case model.Residential:
		objectType = queries.WozObjectTypeResidential
	default:
		return nil, model.ErrUnknownObjectType
	}

	return &queries.WOZObjectCreateParams{
		ID:                  object.ID,
		StakeholderOwner:    ownerID,
		StakeholderOccupant: occupantID,
		AddressableObjectID: object.AddressableObjectID,
		RegisteredPeople:    int32(object.RegisteredPeople),
		Type:                objectType,
	}, nil
}

func ToWOZObjectValue(record *queries.DigilabDemoFwozObjectValue) *model.WOZObjectValue {
	var effectiveAt *time.Time
	if record.EffectiveAt.Valid {
		effectiveAt = &record.EffectiveAt.Time
	}

	return &model.WOZObjectValue{
		ID:          record.ID,
		WOZObjectID: record.WozObjectID,
		Value:       int(record.Value),
		ValuationAt: record.ValuationAt,
		EffectiveAt: effectiveAt,
	}
}

func ToWOZObjectValues(records []*queries.DigilabDemoFwozObjectValue) []model.WOZObjectValue {
	values := make([]model.WOZObjectValue, 0, len(records))
	for idx := range records {
		values = append(values, *ToWOZObjectValue(records[idx]))
	}

	return values
}

func FromWOZObjectValueCreate(object *model.WOZObjectValue) *queries.WOZObjectValueCreateParams {
	var effectiveAt sql.NullTime
	if object.EffectiveAt != nil {
		effectiveAt = sql.NullTime{Time: *object.EffectiveAt, Valid: true}
	}

	return &queries.WOZObjectValueCreateParams{
		ID:          object.ID,
		WozObjectID: object.WOZObjectID,
		Value:       int32(object.Value),
		ValuationAt: object.ValuationAt,
		EffectiveAt: effectiveAt,
	}
}

func FromStakeholderCreate(object *model.Stakeholder) *queries.StakeholderCreateParams {
	var shType model.StakeholderType
	if object.BSN != "" {
		shType = model.NaturalPerson
	} else if object.RSIN != "" {
		shType = model.NonNaturalPerson
	}

	return &queries.StakeholderCreateParams{
		ID:   object.ID,
		Bsn:  sql.NullString{String: object.BSN, Valid: shType == model.NaturalPerson},
		Rsin: sql.NullString{String: object.RSIN, Valid: shType == model.NonNaturalPerson},
		Type: string(shType),
	}
}
