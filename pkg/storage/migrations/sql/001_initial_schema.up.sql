BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS digilab_demo_fwoz;

CREATE TABLE digilab_demo_fwoz.interested_parties (
    id UUID NOT NULL,
    bsn TEXT,
    rsin TEXT,

    type TEXT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_fwoz.woz_objects (
    id UUID NOT NULL,

    interested_owner UUID,
    interested_party UUID,

    addressable_object_id UUID NOT NULL,

    created_at TIMESTAMP DEFAULT NOW(),

    PRIMARY KEY(id),

    CONSTRAINT fk_interested_owner FOREIGN KEY(interested_owner) REFERENCES digilab_demo_fwoz.interested_parties(id),
    CONSTRAINT fk_interested_party FOREIGN KEY(interested_party) REFERENCES digilab_demo_fwoz.interested_parties(id)
);


CREATE TABLE digilab_demo_fwoz.object_value (
    id UUID NOT NULL,
    woz_object_id UUID NOT NULL,

    value INT NOT NULL,
    valuation_at TIMESTAMP NOT NULL,
    effective_at TIMESTAMP,

    PRIMARY KEY(id),

    CONSTRAINT fk_woz_object FOREIGN KEY(woz_object_id) REFERENCES digilab_demo_fwoz.woz_objects(id)
);

COMMIT;
